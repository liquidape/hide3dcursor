# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Hide3DCursor",
    "author": "Sean Olson",
    "version": (1, 0),
    "blender": (2, 71, 0),
    "location": "Search->\"Hide 3D Cursor\"",
    "description": "Add Operator to hide 3d cursor",
    "warning": "",
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.6/Py/"
                "Scripts/3D_interaction/CardinalMouse",
    "tracker_url": "",
    "category": "3D View"}

import bpy
def findSpace():
        area = None
        for area in bpy.data.window_managers[0].windows[0].screen.areas:
            if area.type == 'VIEW_3D':
                break
        if area.type != 'VIEW_3D':
            return None
        for space in area.spaces:
            if space.type == 'VIEW_3D':
                break
        if space.type != 'VIEW_3D':
            return None
        return space

def setCursor(coordinates):
        spc = findSpace()
        spc.cursor_location = coordinates

    
def getCursor(cls):
        spc = cls.findSpace()
        return spc.cursor_location

class VIEW3D_OT_hide_cursor(bpy.types.Operator):
        bl_idname = "view3d.hide_cursor"
        bl_label = "Hide 3D Cursor"
        bl_options = {'REGISTER'}

        def modal(self, context, event):
                return {'FINISHED'}

        def execute(self, context):
                setCursor([1e100,1e100,1e100])
                return {'FINISHED'}

    
def register():
        bpy.utils.register_class(VIEW3D_OT_hide_cursor)

def unregister():
        return {'FINISHED'}
                            




